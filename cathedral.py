PLAYER_POS: Position = None
r = FillOperation.REPLACE
br = BRICKS
ob = OBSIDIAN

def _build_line(material, x_length, x_offset = 0, y = 0, z = 3):
    blocks.fill(material, 
    positions.add(PLAYER_POS, pos(x_offset, y, z)),
    positions.add(PLAYER_POS, pos(x_offset + x_length - 1, y, z)), r)

def _build_square_horizontal(material, x_length, x_offset, y, z_length, z_offset = 0):
    for i in range(z_length):
        _build_line(material, x_length, x_offset, y, z_offset + i)

def _build_cube(material, x_length, x_offset, y_length, y_offset, z_length, z_offset):
    for i in range(y_length):
        _build_square_horizontal(material, x_length, x_offset, y_offset + i, z_length, z_offset)


def __make_mariacki():

    player.say("Making mariacki...")
    # base
    _build_cube(br, x_length = 15, x_offset = 0, y_length = 7, y_offset = 0, z_length = 9, z_offset = 0)
    # left tower
    _build_cube(br, x_length = 3, x_offset = 0, y_length = 14, y_offset = 0, z_length = 3, z_offset = 0)
    _build_cube(br, x_length = 1, x_offset = 1, y_length = 2, y_offset = 14, z_length = 1, z_offset = 1)
    _build_cube(ob, x_length = 1, x_offset = 1, y_length = 1, y_offset = 16, z_length = 1, z_offset = 1)
    # right tower
    _build_cube(br, x_length = 3, x_offset = 0, y_length = 14, y_offset = 0, z_length = 3, z_offset = 6)
    _build_cube(ob, x_length = 3, x_offset = 0, y_length = 1, y_offset = 14, z_length = 3, z_offset = 6)
    _build_cube(ob, x_length = 1, x_offset = 1, y_length = 1, y_offset = 14, z_length = 1, z_offset = 7)
    # roof
    _build_square_horizontal(br, x_length = 12, x_offset = 3, y = 7, z_length = 9, z_offset = 0)
    _build_square_horizontal(br, x_length = 11, x_offset = 3, y = 8, z_length = 7, z_offset = 1)
    _build_square_horizontal(br, x_length = 10, x_offset = 3, y = 9, z_length = 5, z_offset = 2)
    _build_square_horizontal(br, x_length = 9, x_offset = 3, y = 10, z_length = 3, z_offset = 3)
    _build_square_horizontal(br, x_length = 8, x_offset = 3, y = 11, z_length = 1, z_offset = 4)
    
    #entrance
    _build_cube(br, x_length = 2, x_offset = -2, y_length = 3, y_offset = 0, z_length = 5, z_offset = 2)
    _build_cube(br, x_length = 1, x_offset = -3, y_length = 3, y_offset = 0, z_length = 3, z_offset = 3)
    _build_cube(ob, x_length = 2, x_offset = -2, y_length = 2, y_offset = 3, z_length = 3, z_offset = 3)

    # right nave
    _build_cube(br, x_length = 10, x_offset = 3, y_length = 3, y_offset = 0, z_length = 3, z_offset = -3)
    _build_square_horizontal(ob, x_length = 10, x_offset = 3, y = 3, z_length = 2, z_offset = -2)
    _build_square_horizontal(ob, x_length = 10, x_offset = 3, y = 4, z_length = 1, z_offset = -1)
    # left nave
    _build_cube(br, x_length = 10, x_offset = 3, y_length = 3, y_offset = 0, z_length = 3, z_offset = 9)
    _build_square_horizontal(ob, x_length = 10, x_offset = 3, y = 3, z_length = 2, z_offset = 9)
    _build_square_horizontal(ob, x_length = 10, x_offset = 3, y = 4, z_length = 1, z_offset = 9)

def make_mariacki():
    global PLAYER_POS
    PLAYER_POS = player.position()
    gameplay.set_weather(CLEAR)
    gameplay.time_set(gameplay.time(DAY))
    player.teleport(positions.add(PLAYER_POS, pos(0, 0, -5)))
    __make_mariacki()
    player.say("Finished!")

def say_pos():
    player.say(player.position())


player.on_chat("p", say_pos)
player.on_chat("mar", make_mariacki)
